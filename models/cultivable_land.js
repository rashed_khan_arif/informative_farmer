const mongoose = require("mongoose");
const userSchema = mongoose.Schema({
    cultivableLand: {
        type: String,
        required: true,
        trim: true
    },
    unit: {
        type: String,
        required: true,
        trim: true
    },
    session: {
        type: Number,
        required: true
    }

});
module.exports = mongoose.model("CultivableLand", userSchema);