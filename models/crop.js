const mongoose = require("mongoose");
const userSchema = mongoose.Schema({
    cropName: {
        type: String,
        required: true,
        trim: true
    },
    session: {
        type: String,
        required: true,
        trim: true
    },
    landArea: {
        type: String,
        required: true
    },
    initInvestment: {
        type: String,
        required: true,
        trim: true
    },
    startDate: {
        type: String,
        required: true,
        trim: true
    },
    endDate: {
        type: String,
        required: true,
        trim: true
    },
    note: {
        type: String,
        required: true,
        trim: true
    },
    unit: {
        type: String,
        required: true,
        trim: true
    }
});
const user = module.exports = mongoose.model("Crop", userSchema);