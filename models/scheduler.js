const mongoose = require("mongoose");
const userSchema = mongoose.Schema({
    session: {
        type: String,
        required: true,
        trim: true
    },
    cropId: {
        type: String,
        required: true,
        trim: true
    },
    cropName: {
        type: String
    },
    date: {
        type: String,
        required: true
    },
    note: {
        type: String,
        required: true,
        trim: true
    }
});
const Schduler = module.exports = mongoose.model("Schduler", userSchema);