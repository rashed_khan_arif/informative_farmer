var express = require("express");
var cors = require("cors");
const db = require("./db/mongoose");
var { ObjectId } = require("mongodb");
var esession = require('express-session');
var bodyParser = require("body-parser");
var path = require("path");
var events = require("events");
var app = express();
const port = 3000;
var hbs = require("hbs");
var session = require("./models/session");
var crop = require("./models/crop");
var user = require("./models/user");
var Schduler = require("./models/scheduler");
var CultivableLand = require("./models/cultivable_land");
var empty = require('is-empty');
const evnt = new events.EventEmitter();
app.set("view engine", "hbs");
app.set('views', path.join(__dirname, 'views'));

hbs.registerPartials(__dirname + "/views/partials")
app.use(cors());
//app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(esession({ secret: 'informativefarmer', saveUninitialized: false, resave: false, expires: new Date(Date.now() + (30 * 86400 * 1000)) }));
//request handle area
var isLogin = false;
app.use(function (req, res, next) {
    res.locals.isLogin = isLogin;
    next();
});

app.get('/', (req, res) => {
    console.log(isLogin);
    res.render('index.hbs', {
        title: "Home | Informative Farmer", whichPartial: function () {
            return "home";
        }
    });
});


//land area start
app.get('/land-setup', (req, res) => {
    console.log(req.session);
    res.render('index.hbs', {
        title: "Land Setup", whichPartial: function () {
            return "land_setup";
        }, session: session.SessionEnum, success: req.session.success, error: req.session.error,
    });
});
app.post('/create-land', (req, res) => {
    console.log("Create Land: " + req);
    var totalCultivable = req.body.cultivableland;
    var sessionId = req.body.session;
    var landUnit = req.body.unit;
    let cl = new CultivableLand({
        cultivableLand: totalCultivable,
        session: sessionId,
        unit: landUnit
    });
    console.log(JSON.stringify(cl, undefined, 2));
    cl.save().then((doc) => {
        req.session.success = "Land successfully added !";
        res.redirect("/land-setup");
    }).catch((er) => {
        req.session.error = er;
        res.send("Failed to setup land " + er);
    })
});
app.get('/land-list', (req, res) => {
    res.render('index.hbs', {
        title: "Land List", whichPartial: function () {
            return "land_list";
        }, session: session.SessionEnum,
    });
});

//land area end 

//crop area
app.get('/crop-setup', (req, res) => {
    res.render('index.hbs', {
        title: "Crop Setup", whichPartial: function () {
            return "crop_setup";
        }, session: session.SessionEnum,
    });
});
app.post("/create-crop", (req, res) => {
    let crp = new crop({
        cropName: req.body.cropName,
        landArea: req.body.landArea,
        session: req.body.session,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        note: req.body.note,
        initInvestment: req.body.initInvestment,
        unit: req.body.unit
    });
    console.log(JSON.stringify(crp, undefined, 2));
    console.log("Crop Setup");
    crp.save().then((doc) => {
        req.session.success = "Crop successfully added !";
        res.redirect("/crop-setup")
    }).catch((err) => {
        res.send(err);
    });
});
app.get("/crop-list", (req, res) => {
    res.render('index.hbs', {
        title: "Crop List", whichPartial: function () {
            return "crop_list";
        }, session: session.SessionEnum,
    });
});

//only for ajax call
app.get("/search-crop/:session", (req, res) => {
    var sessionId = req.params.session;
    console.log(sessionId);
    crop.find({ session: sessionId }).then((docs => {
        console.log(JSON.stringify(docs, undefined, 2));
        res.send(docs);
    })).catch(err => {
        console.log(err);
        res.send("failed!");
    });
});
app.get("/edit-crop/:id", (req, res) => {
    var id = req.params.id;
    if (!ObjectId.isValid(id)) {
        return res.status(404).send("Invalid Id");
    }
    var _id = new ObjectId(id);
    crop.findById(_id).then((crop) => {
        if (!crop) {
            return res.status(404).send(crop);
        }
        console.log(JSON.stringify(crop, undefined, 2));
        res.render('index.hbs', {
            title: "Edit crop", whichPartial: function () {
                return "edit_crop";
            }, session: session.SessionEnum, crop: crop
        });
    }).catch(err => res.status(404).send(err));;



});
app.post("/update-crop", (req, res) => {
    var id = new ObjectId(req.body.id);

    let crp = new crop({
        cropName: req.body.cropName,
        landArea: req.body.landArea,
        session: req.body.session,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        note: req.body.note,
        initInvestment: req.body.initInvestment,
        unit: req.body.unit
    });
    console.log(JSON.stringify(crp, undefined, 2));

    crop.findOneAndUpdate({ _id: id }, {
        $set: {
            cropName: crp.cropName,
            landArea: crp.landArea,
            session: crp.session,
            startDate: crp.startDate,
            endDate: crp.endDate,
            note: crp.note,
            unit: crp.unit,
            initInvestment: crp.initInvestment
        }
    }, { new: true }).then((doc) => {
        req.session.success = "Crop successfully update !";
        res.redirect("/edit-crop/" + id)
    }).catch((err) => {
        res.send(err);
    });
});
//end crop


app.get('/report', (req, res) => {
    res.render('index.hbs', {
        title: "Report ", whichPartial: function () {
            return "report";
        }, session: session.SessionEnum,
    });
});


//scheduler start
app.get('/create-scheduler', (req, res) => {
    res.render('index.hbs', {
        title: "Scheduler ", whichPartial: function () {
            return "create_scheduler";
        }, session: session.SessionEnum, success: req.session.success, error: req.session.error
    });
});
app.post("/save-scheduler", (req, res) => {
    let scheduler = new Schduler({
        cropId: req.body.cropId,
        session: req.body.session,
        date: req.body.date,
        note: req.body.note
    });
    console.log(JSON.stringify(scheduler, undefined, 2));
    console.log("Create  Scheduler");
    scheduler.save().then((doc) => {
        req.session.success = "Schedule  successfully added !";
        res.redirect("/create-scheduler")
    }).catch((err) => {
        res.send(err);
    });
});
app.get('/schedulers', (req, res) => {
    res.render('index.hbs', {
        title: "Scheduler ", whichPartial: function () {
            return "scheduler";
        }, session: session.SessionEnum,
    });
});
app.get("/search-sch/:session", (req, res) => {
    var session = req.params.session;
    Schduler.find({ session: session }).then((docs => {
        res.send(docs);
    })).catch(err => {
        console.log(err);
        res.send("failed!");
    });
});
//scheduler end
//login
app.get('/login', (req, res) => {
    res.render('index.hbs', {
        title: "Login ", whichPartial: function () {
            return "login";
        }, session: session.SessionEnum, error: req.session.error
    });
});

app.post("/do-login", (req, res) => {
    var email = req.body.email;
    var pass = req.body.password;
    user.find({ email: email, password: pass }).then((doc) => {
        console.log(doc);
        if (!empty(doc)) {
            isLogin = true;
            res.redirect("/");
        } else {
            req.session.error = "Invalid login information !";
            req.session.setAt = false;
            res.redirect("/login");
        }
    }).catch(err => {
        isLogin = false;
        console.log(err);
        req.session.error = "Invalid login information !";
        res.redirect("/login");
    });
});
app.get("/logout", (req, res) => {
    isLogin = false;
    res.redirect("/");
});
//end login
//start sign up
app.get("/sign-up", (req, res) => {
    res.render('index.hbs', {
        title: "Sign up ", whichPartial: function () {
            return "sign_up";
        }, session: session.SessionEnum, error: req.session.error
    });
});
app.post("/do-signup", (req, res) => {
    var email = req.body.email;
    var pass = req.body.password;
    var name = req.body.name;
    var contactNo = req.body.contactNo;
    let u = new user({
        name: name,
        email: email,
        password: pass,
        contactNo: contactNo
    });
    u.save().then((doc) => {
        res.redirect("/");
    }).catch((err) => {
        console.log(err);
        req.session.error = "failed!";
        res.redirect("/sign-up");
    });

});
//end sign up

//end request handle area
app.listen(port, (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("Server started at port " + port);
});
