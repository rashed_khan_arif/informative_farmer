const express = require("express");
const router = express.Router();
const db = require("../db/mongoose");
const User = require("../models/user");
router.get("/users", (req, res, next) => {
    User.find().then((users) => {
        res.send(users);
    }).catch((er) => {
        res.send(er);
    });
});
router.post("/user", (req, res) => {
    console.log("request for add user")
    let user = new User({
        fullName: req.body.fullName,
        email: req.body.email,
        contactNo: req.body.contactNo,
        userName: req.body.userName
    });
    user.save().then((doc) => {
        res.send({ msg: "User is added !" });
    }).catch((er) => {
        res.send({ msg: "Error on add user !" + er })
    })
});


module.exports = router;
