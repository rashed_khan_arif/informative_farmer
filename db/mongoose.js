var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/informative_farmer');
mongoose.connection.on("error", (err) => {
    return console.log("Failed to connect with DB" + err);
});


module.exports = { mongoose }